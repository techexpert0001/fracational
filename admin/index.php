<?php
include 'config.php';
?>

<?php
if (isset($_SESSION['username']) and ! empty($_SESSION['username'])) {
    include 'header.php';
    ?>



    <h1 class="page-header">Contact List</h1>


    <?php
    $query = "SELECT * FROM `contact`";
    $result = mysqli_query($con, $query) or die(mysql_error());
    ?>
    <table class="table">
        <thead>
            <tr>
                <th>S.no</th>
                <th>Firstname</th>
                <th>Lastname</th>
                <th>Email</th> 
                <th>Apple ID</th>
                <th>Subject</th> 
                <th>Message</th>
                <th>Datetime</th>
                <th>Ip</th>   
                <th>Action</th>     
            </tr>
        </thead>
        <tbody>

    <?php
    if (mysqli_num_rows($result)) {

        $count = 0;
        while ($row = mysqli_fetch_array($result, MYSQLI_ASSOC)) {

            $count++;
            ?>

                    <tr>
                        <td><?php echo $count; ?></td>
                        <td>  <?php echo $row['firstname']; ?></td>
                        <td>  <?php echo $row['lastname']; ?></td>
                        <td>  <?php echo $row['email']; ?></td>
                         <td>  <?php echo $row['apple_id']; ?></td>
                        
                        <td>  <?php echo $row['subject']; ?></td>
                        <td>  <?php if (isset($row['message'])) {
                custom_echo($row['message'], 100);
            } ?></td>             
                        <td><?php if (isset($row['datetime'])) {
                $tm = strtotime($row['datetime']);
                echo date('F d, Y h:i A', $tm);
            } ?></td>
                        <td>  <?php echo $row['ip_address']; ?></td>
                        <td><a href="list.php?contact=1&view=<?php echo $row['id']; ?>">View</a>&nbsp;<a onclick="if (confirm(('Are you Sure?'))) {
                                window.location = 'list.php?contact=1&delete=<?php echo $row['id']; ?>';
                            }" >Delete</a></td>
                    </tr>
        <?php }
    } ?>


        </tbody>
    </table> 


    <?php
    include 'footer.php';
} else {
    ?>
    <!DOCTYPE html>
    <html lang="en" class="no-js">
        <!--<![endif]-->
        <head>
            <meta charset="UTF-8" />
            <title>Login</title>
            <link href="style.css" rel="stylesheet">
        </head>
        <body>


            <div class="container">
                <div id="login-box">
                    <div class="logo">
                        <img src="../img/logo_dark_nav.png" class="img img-responsive img-circle center-block"/>
                        <h1 class="logo-caption"><span class="tweak">L</span>ogin</h1>
                    </div><!-- /.logo -->
                    <div class="controls">
                        <form  action="verify.php" autocomplete="on" method="post">
                            <input type="hidden" value="1" name="login">
                            <input id="username" name="username" required type="text" placeholder="Username" class="form-control" />
                            <input id="password" name="password" required type="password" placeholder="Password" class="form-control"/>			 
                            <button type="submit" class="btn btn-default btn-block btn-custom">Login</button>
                        </form>
                    </div><!-- /.controls -->
                </div><!-- /#login-box -->
            </div><!-- /.container -->
            <div id="particles-js"></div>
            <!--<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/particles.js/2.0.0/particles.min.js"></script>-->



            <link href="css/bootstrap.min.css" rel="stylesheet">
            <style>
                @import url('https://fonts.googleapis.com/css?family=Nunito');
                @import url('https://fonts.googleapis.com/css?family=Poiret+One');

                body, html {
                    height: 100%;
                    background-repeat: no-repeat;    /*background-image: linear-gradient(rgb(12, 97, 33),rgb(104, 145, 162));*/
                    background:black;
                    position: relative;
                }
                #login-box {
                    position: absolute;
                    top: 0px;
                    left: 50%;
                    transform: translateX(-50%);
                    width: 350px;
                    margin: 0 auto;
                    border: 1px solid black;
                    background: rgba(48, 46, 45, 1);
                    min-height: 250px;
                    padding: 20px;
                    z-index: 9999;
                }
                #login-box .logo .logo-caption {
                    font-family: 'Poiret One', cursive;
                    color: white;
                    text-align: center;
                    margin-bottom: 0px;
                }
                #login-box .logo .tweak {
                    color: #ff5252;
                }
                #login-box .controls {
                    padding-top: 30px;
                }
                #login-box .controls input {
                    border-radius: 0px;
                    background: rgb(98, 96, 96);
                    border: 0px;
                    color: white;
                    font-family: 'Nunito', sans-serif;
                }
                #login-box .controls input:focus {
                    box-shadow: none;
                }
                #login-box .controls input:first-child {
                    border-top-left-radius: 2px;
                    border-top-right-radius: 2px;
                }
                #login-box .controls input:last-child {
                    border-bottom-left-radius: 2px;
                    border-bottom-right-radius: 2px;
                }
                #login-box button.btn-custom {
                    border-radius: 2px;
                    margin-top: 8px;
                    background:#ff5252;
                    border-color: rgba(48, 46, 45, 1);
                    color: white;
                    font-family: 'Nunito', sans-serif;
                }
                #login-box button.btn-custom:hover{
                    -webkit-transition: all 500ms ease;
                    -moz-transition: all 500ms ease;
                    -ms-transition: all 500ms ease;
                    -o-transition: all 500ms ease;
                    transition: all 500ms ease;
                    background: rgba(48, 46, 45, 1);
                    border-color: #ff5252;
                }
                #particles-js{
                    width: 100%;
                    height: 100%;
                    background-size: cover;
                    background-position: 50% 50%;
                    position: fixed;
                    top: 0px;
                    z-index:1;
                }
            </style>
            <script src="https://code.jquery.com/jquery-1.10.2.min.js"></script>
            <script>
                    $.getScript("https://cdnjs.cloudflare.com/ajax/libs/particles.js/2.0.0/particles.min.js", function () {
                        particlesJS('particles-js',
                                {
                                    "particles": {
                                        "number": {
                                            "value": 80,
                                            "density": {
                                                "enable": true,
                                                "value_area": 800
                                            }
                                        },
                                        "color": {
                                            "value": "#ffffff"
                                        },
                                        "shape": {
                                            "type": "circle",
                                            "stroke": {
                                                "width": 0,
                                                "color": "#000000"
                                            },
                                            "polygon": {
                                                "nb_sides": 5
                                            },
                                            "image": {
                                                "width": 100,
                                                "height": 100
                                            }
                                        },
                                        "opacity": {
                                            "value": 0.5,
                                            "random": false,
                                            "anim": {
                                                "enable": false,
                                                "speed": 1,
                                                "opacity_min": 0.1,
                                                "sync": false
                                            }
                                        },
                                        "size": {
                                            "value": 5,
                                            "random": true,
                                            "anim": {
                                                "enable": false,
                                                "speed": 40,
                                                "size_min": 0.1,
                                                "sync": false
                                            }
                                        },
                                        "line_linked": {
                                            "enable": true,
                                            "distance": 150,
                                            "color": "#ffffff",
                                            "opacity": 0.4,
                                            "width": 1
                                        },
                                        "move": {
                                            "enable": true,
                                            "speed": 6,
                                            "direction": "none",
                                            "random": false,
                                            "straight": false,
                                            "out_mode": "out",
                                            "attract": {
                                                "enable": false,
                                                "rotateX": 600,
                                                "rotateY": 1200
                                            }
                                        }
                                    },
                                    "interactivity": {
                                        "detect_on": "canvas",
                                        "events": {
                                            "onhover": {
                                                "enable": true,
                                                "mode": "repulse"
                                            },
                                            "onclick": {
                                                "enable": true,
                                                "mode": "push"
                                            },
                                            "resize": true
                                        },
                                        "modes": {
                                            "grab": {
                                                "distance": 400,
                                                "line_linked": {
                                                    "opacity": 1
                                                }
                                            },
                                            "bubble": {
                                                "distance": 400,
                                                "size": 40,
                                                "duration": 2,
                                                "opacity": 8,
                                                "speed": 3
                                            },
                                            "repulse": {
                                                "distance": 200
                                            },
                                            "push": {
                                                "particles_nb": 4
                                            },
                                            "remove": {
                                                "particles_nb": 2
                                            }
                                        }
                                    },
                                    "retina_detect": true,
                                    "config_demo": {
                                        "hide_card": false,
                                        "background_color": "#b61924",
                                        "background_image": "",
                                        "background_position": "50% 50%",
                                        "background_repeat": "no-repeat",
                                        "background_size": "cover"
                                    }
                                }
                        );

                    });






            </script>
        </body>
    </html>
<?php } ?>