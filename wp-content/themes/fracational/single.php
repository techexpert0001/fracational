<?php
/**
 * The template for displaying all single posts and attachments
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
 
get_header();  
?>
<div class="fullwidth-section" id="blog-small-img-sidebar">
<div class="container">
<?php
 $cate=get_the_category(get_the_ID());
 
 
$useragent=$_SERVER['HTTP_USER_AGENT'];
  $id = get_the_ID();
 $getLoopDdata=get_post(get_the_ID()); 
 $linkd=  get_post_permalink($getLoopDdata->ID) ;
 

/* $getLoop=query_posts(array(
    'category_name' => 'blog', // get posts by category name
    'posts_per_page' => -1 // all posts
)); */

  if($cate[0]->cat_ID==3){

 
 //$the_post = get_post($id);
//echo $the_post->post_password;


?>	<!-- video Area -->

<!-- =============================================== -->
<!-- ============== START FULLWIDTH BLOG ============== -->
<!-- =============================================== -->
 
<div class="row" style="margin-bottom: 40px;">
<div class="col-md-8 col-md-offset-2 text-center">
<h1 class="weight-800 uppercase"><?php echo $getLoopDdata->post_title;?></h1>
<ol class="breadcrumb">
  <li><a href="<?php echo bloginfo('url'); ?>">Home</a></li>
  <li class="active">Blog</li>
</ol>
<hr/>
</div>
</div>
<div class="row">
<!-- Blog Posts Section -->
<div class="col-md-12">
<!-- Blog Post -->

<?php 
    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $getLoopDdata->ID ),  array(300,300) );
  ?>
<div class="row">
<div class="col-md-6">
<div class="blog-img-wrapper">
<div class="blog-img-hover">
<div class="hover-left"><a href="<?php echo $linkd;?>"><i class="im-redo2"></i></a></div>
<div class="hover-right"><a data-pp="prettyPhoto[blog-gallery]" href="<?php echo $image[0]; ?>" title=""><i class="im-expand2"></i></a></div>
</div>
<img alt="" src="<?php echo $image[0]; ?>" class="img-responsive">
</div>
</div>
<div class="col-md-6">
<h4><a href="<?php echo $linkd;?>"><?php echo $getLoopDdata->post_title;?></a></h4>
<h5>by CoeqWEty on <?php $d=  strtotime($getLoopDdata->post_date); echo date('F d, Y',$d); ?></h5>
<p> 
<?php 
//echo get_the_content($getLoopDdata->ID);
//$string = strip_tags($getLoopDdata->post_content);


 

$string = $getLoopDdata->post_content;

if(!get_the_content($getLoopDdata->ID)){
echo do_shortcode($string);
}else{

	echo get_the_content($getLoopDdata->ID);
}
/* if (strlen($string) > 500) {



  

    $stringCut = substr($string, 0, 500);



    

    $string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...'; 

}*/ //echo $string; 
?> </p>
<?php /* <h5>Posted in Blog | 23 Comments</h5> */ ?>
</div>
<div class="col-md-12">
<hr/>
</div>
</div>
<?php
  ?>
 
 
 <?php /*
<div class="row">
<div class="col-md-12">
<ul class="pagination">
  <li><a href="#">Previous</a></li>
  <li><a href="#">1</a></li>
  <li class="active"><a href="#">2</a></li>
  <li><a href="#">3</a></li>
  <li><a href="#">4</a></li>
  <li><a href="#">5</a></li>
  <li><a href="#">Next</a></li>
</ul>
</div>
</div> */ ?>
</div>

 


</div>

  <?php }elseif($cate[0]->cat_ID==5){
      
  ?>

 <iframe src="https://docs.google.com/gview?url=http://www.fracational.com/wp-content/uploads/2017/06/sample.ppt&embedded=true" style="width:100%; height:500px;" frameborder="0"></iframe>
 


<?php     
      
  }else{ ?>
      
      

<div class="row" style="margin-bottom: 40px;">
<div class="col-md-8 col-md-offset-2 text-center">
<h1 class="weight-800 uppercase"><?php echo $getLoopDdata->post_title;?></h1>
<ol class="breadcrumb">
  <li><a href="<?php echo bloginfo('url'); ?>">Home</a></li>
  <li class="active">Post</li>
</ol>
<hr/>
</div>
</div>
    
    
<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->

	<?php twentysixteen_post_thumbnail(); ?>

	<div class="entry-content">
		<?php
		the_content();

		wp_link_pages( array(
			'before'      => '<div class="page-links"><span class="page-links-title">' . __( 'Pages:', 'twentysixteen' ) . '</span>',
			'after'       => '</div>',
			'link_before' => '<span>',
			'link_after'  => '</span>',
			'pagelink'    => '<span class="screen-reader-text">' . __( 'Page', 'twentysixteen' ) . ' </span>%',
			'separator'   => '<span class="screen-reader-text">, </span>',
		) );
		?>
	</div><!-- .entry-content -->

	<?php
		edit_post_link(
			sprintf(
				/* translators: %s: Name of current post */
				__( 'Edit<span class="screen-reader-text"> "%s"</span>', 'twentysixteen' ),
				get_the_title()
			),
			'<footer class="entry-footer"><span class="edit-link">',
			'</span></footer><!-- .entry-footer -->'
		);
	?>

</article><!-- #post-## -->
  
  <?php } ?>

</div>
</div>
<?php get_footer(); ?>
