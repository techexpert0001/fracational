// ========== TABLE OF CONTENTS =========== //
//
// 1. PAGE LOADER 
// 2. ACTIVE CLASS
// 3. STYLE SWITCHER
// 4. BOOTSTRAP CAROUSEL SLIDER
// 5. SCROLL TO TOP BUTTON
// 6. TEAM DISPLAY DROP DOWNS
// 7. FADE IN NAVIGATION
// 8. HELPER CLASSES FOR HEIGHTS AND CENTERING
// 9. SCROLL TO SCRIPT USED FOR ONE PAGE THEME
// 10. PARALLAX BACKGROUNDS
// 11. BOOTSTRAP ACCORDION
// 12. OWL SLIDER
// 13. SCROLL TO REPLY SECTION ON POST PAGES
// 14. CLOSE MOBILE MENU ON SELECT
//
// ======================================= //

// ========== 1. START PAGE LOADER ========== //
 (function($) { "use strict";

$(window).load(function() {
	$(".loader-img").delay(500).fadeOut();
	$("#pageloader").delay(1000).fadeOut("slow");
	var hash = window.location.hash;
	if(!hash) { 
	// Do nothing //
	} else {
	$(document).scrollTop( $(hash).offset().top -56); 
}
});
 })(jQuery);
 // ========== END PAGE LOADER ========== //


    // ========== 2. ADDS "active" CLASS TO ANY CLICKED BUTTONS ========== //
    (function($) { "use strict";
 $(document).ready(function(){
  $('a.btn').click(function(){
  $('a.btn').removeClass('active');
  $(this).addClass('active');
  });
});
 })(jQuery);
   // ========== END ACTIVE CLASS ========== // 
	

    // ========== 3. START STYLE SWITCH ========== //
    (function($) { "use strict";
 $(document).ready(function(){
  $('.style-switch-button').click(function(){
  $('.style-switch-wrapper').toggleClass('active');
  });
  $('a.close-styler').click(function(){
  $('.style-switch-wrapper').removeClass('active');
  });
});
 })(jQuery);
   // ========== END STYLE SWITCH ========== //

	   // ========== 4. START BOOTSTRAP CAROUSEL ========== //
	 (function($) { "use strict";
$('.carousel').carousel({
  interval: 5000,
  pause: "hover",
});
 })(jQuery);
		   // ========== END BOOTSTRAP CAROUSEL ========== //
	
 // ========== 5. START SCROLL TO TOP ========== //
 // Button
 (function($) { "use strict";
$(document).ready(function() {
     $(".scrollup").hide();
     $(window).scroll(function() {
         if ($(this).scrollTop() > 400) {
             $('.scrollup').fadeIn();
         } else {
             $('.scrollup').fadeOut();
         }
     });
 });
 })(jQuery);
 // Action
   (function($) { "use strict";
$("a.scrolltotop[href^='#']").on('click', function(e) {

   // prevent default anchor click behavior
   e.preventDefault();

   // store hash
   var hash = this.hash;

   // animate
   $('html, body').stop().animate({scrollTop:0}, 1000, 'easeOutExpo');

});
 })(jQuery);
  // ========== END SCROLL TO TOP ========== //
  
   // ========== 6. START TEAM DISPLAY ========== //
  (function($) { "use strict";
$(document).ready(function() {
     $('.team-drop ul').hide();
      $('.team-drop li i').on('click', function (e) {
	  $(this).css("z-index","10000");
        e.preventDefault();
        var elem = $(this).next('.team-drop ul')
        $('.team-drop ul').not(elem).hide();
        elem.toggle();
		 
    });

 });
 })(jQuery);
   // ========== END TEAM DISPLAY ========== //
 
// ========== 7 START FADE IN NAVIGATION ========== //
 (function($) { "use strict";
$(document).ready(function() {
     $(".nav-fadein").hide();
     $(window).on('scroll load', function() {
         if ($(this).scrollTop() > 400) {
             $('.nav-fadein').fadeIn();
         } else {
             $('.nav-fadein').fadeOut();
         }
     });
 });
 })(jQuery);
// ========== END FADE IN NAVIGATION ========== //

 // ========== 8. HELPER CLASSES FOR HEIGHTS AND CENTERING ========== //
  (function($) { "use strict";
$(document).ready(function() {
     $(window).on('resize load', function() {
       var newheight = $(window).height() * .75;
	   $('.height-75').height(newheight);
     });
 });
 })(jQuery);
 // ===== 100% HEIGHT ===== //
   (function($) { "use strict";
$(document).ready(function() {
     $(window).on('resize load', function() {
       var newheight = $(window).height();
	   $('.height-100').height(newheight);
     });
 });
 })(jQuery);




   (function ($) {
       "use strict";
       $(document).ready(function () {
           $('.FormdataSend').on('click', function () {
               var firstname = $('#fname').val();
               var lastname = $('#lname').val();
               var email = $('#email').val();
               var subject = $('#subject').val();
               var message = $('#message').val();
               var apple_email = $('#apple_email').val();

               if (firstname !== "" && lastname !== "" && isValidEmailAddress(email) && subject !== "" && message !== "") {
                 // alert('testing'); 
                   $.ajax({
                     url: "send.php",
                       dataType: "jsonp",
                       type: 'POST',
                       data: {
                           fname: firstname,
                           lname: lastname,
                           email: email,
                           subject: subject,
                           message: message,
                           appleemail: apple_email,
                           s: 'active',
                       },
                       success: function (response) {
                          // alert('fd');
                           $('#formverifyrequest input[type=text],#formverifyrequest input[type=email],#formverifyrequest textarea').css('border', '1px solid #ddd');
                           $('.emailformresponse').css('display', 'block');
                           $('.emailformresponse').removeClass('alert-danger');
                           $('.emailformresponse').addClass('alert-success');
                           $('.emailformresponse').html('Thank you for contacting us');

                           $('#formverifyrequest input[type=text],#formverifyrequest input[type=email],#formverifyrequest textarea').val('');
                           
                           
                          
                       },
                       error: function (xhr, ajaxOptions, ThrownError) {
                             $('#formverifyrequest input[type=text],#formverifyrequest input[type=email],#formverifyrequest textarea').css('border', '1px solid #ddd');
                           $('.emailformresponse').css('display', 'block');
                           $('.emailformresponse').removeClass('alert-danger');
                           $('.emailformresponse').addClass('alert-success');
                          // $('.emailformresponse').html('Email Sent..');
                         $('.emailformresponse').html('Thank you for contacting us');
                           $('#formverifyrequest input[type=text],#formverifyrequest input[type=email],#formverifyrequest textarea').val('');
                       }

                   });

                  


               } else {
            	 //  alert('123');
                   $('#formverifyrequest input[type=text],#formverifyrequest input[type=email],#formverifyrequest textarea').css('border', '1px solid #ff0000');

                   $('.emailformresponse').css('display', 'block');
                   $('.emailformresponse').removeClass('alert-success');
                   $('.emailformresponse').addClass('alert-danger');
                   $('.emailformresponse').html('Please Fill All the Fields');

                   if (firstname !== "") {
                       $('#formverifyrequest #fname').css('border', '1px solid #ddd');
                   }  if (lastname !== "") {
                       $('#formverifyrequest #lname').css('border', '1px solid #ddd');
                   }  if (subject !== "") {
                       $('#formverifyrequest #subject').css('border', '1px solid #ddd');
                   }
                    if (message !== "") {
                       $('#formverifyrequest #message').css('border', '1px solid #ddd');
                   } 
                    $('#formverifyrequest #apple_email').css('border', '1px solid #ddd');
				  /* if (appleemail !== "") {
                       $('#formverifyrequest #apple_email').css('border', '1px solid #ddd');
                   }*/
                    if (isValidEmailAddress(email)) {
                        $('#formverifyrequest #email').css('border', '1px solid #ddd');
                       // $('.emailformresponse').html('Please Enter Valid Email Address');
                   }

                   
                  
                 
               }
               
             
              
               /*
               $.ajax({
                  url: '',
                   type: 'POST',
                   headers: { 'Access-Control-Request-Headers: x-requested-with' },                  
                   data: {
                       fname: firstname,
                       lname: lastname,
                       email: email,
                       subject: subject,
                       message: message,
                      // s:'active'
                   },
                   success: function (data) {
                       alert(data);

                      
                   }
               });
               */


            

           });
       });
   })(jQuery);


   function isValidEmailAddress(emailAddress) {
       var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
       return pattern.test(emailAddress);
   };


  // ===== DYNAMICALLY VERTICAL CENTER ===== //
     (function($) { "use strict";

     $(window).on('resize load', function() {
	  $('.vertical-center').each(function() {
       var windowheight = $(window).height();
	   var contentheight = $(this).height();
	   var margins = (windowheight - contentheight)/2+'px';
	   $(this).css('margin-top', margins);
     });
	  });

 })(jQuery);
// ========== 9. START SCROLLTO SCRIPT ========== //
     (function($) { "use strict";
         $("a.scrollto[href^='#']").on('click', function (e) {

   // prevent default anchor click behavior
   e.preventDefault();

   // store hash
   var hash = this.hash;
   

   // animate
   $('html, body').stop().animate({
 
       scrollTop: $(hash).offset().top -56}, 1000, 'easeOutExpo');

});
 })(jQuery);
 // ========== END SCROLL TO SCRIPT ========== //

  // ========== 10. START PARALLAX SETTINGS ========== //
 (function($) { "use strict";
				$('.parallax').stellar();
			$.stellar({
				horizontalScrolling: false,
				verticalOffset: 0
			});
		 })(jQuery);
  // ========== END PARALLAX SETTINGS ========== //
  
    // ========== 11. BOOTSTRAP ACCORDION SETTINGS ========== //
(function($) { "use strict";
var $accordion = $('#accordion .panel a');
$accordion.click(function(){
   $accordion.removeClass('selected');
   $(this).addClass('selected');
});
$('#accordion .panel a').on('click',function(e){
    if($(this).parents('.panel').children('.panel-collapse').hasClass('in')){
     $accordion.removeClass('selected');
    }
});
})(jQuery);
// ========= END BOOTSTRAP ACCORDION SETTINGS ========== //

// ========== 12. START OWL SLIDER SETTINGS ========== //
	    $(document).ready(function() {
     
    $("#team-slider").owlCarousel({
items: 4,
itemsScaleUp: true,
autoPlay: false,
stopOnHover: true
});
});
// ========== END OWL SLIDER SETTINGS ========== //
 
// ========== 13. SCROLL TO REPLY SECTION ON BLOG ========== // 
  (function($) { "use strict";
		$(".go-to-reply").click(function() {
     $('html, body').animate({
         scrollTop: $("#reply").offset().top -60 }, 600);
 });
 })(jQuery);
// ========== SCROLL TO REPLY SECTION ON BLOG ========== //

    // ========== 14. CLOSE MOBILE MENU ON SELECT ========== //
    (function($) { "use strict";
 $(document).ready(function(){
  $('.nav li a').click(function(){
  $('.navbar-collapse').removeClass('in');
  });
   $('.nav li.dropdown a').click(function(){
  $('.navbar-collapse').addClass('in');
  });
});
 })(jQuery);
   // ========== END CLOSE MOBILE MENU ON SELECT ========== //