<?php
/**
 * The template part for displaying content
 * Template Name: Blog
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
 
get_header();

$useragent=$_SERVER['HTTP_USER_AGENT'];

  
$getLoop=query_posts(array(
    'category_name' => 'blog', // get posts by category name
    'posts_per_page' => -1 // all posts
));

?>	<!-- video Area -->

<!-- =============================================== -->
<!-- ============== START FULLWIDTH BLOG ============== -->
<!-- =============================================== -->
<div class="fullwidth-section" id="blog-small-img-sidebar">
<div class="blog_header_img"> <!--img src="<?php //echo bloginfo('template_url');?>/assets/img/blog_header_img.png" /-->
<div class="overlay_blog_page"></div>
<div class="container">
<div class="row">
<div style="margin-top:20%;" class="col-md-8 col-md-offset-2 blog_page_bg_title text-center text-white">
<!--h1 style="color:#fff;" class="weight-800 uppercase">Welcome to our Blog</h1-->
<h1 style="color:#fff;" class="weight-800 uppercase">Welcome to Our Knowledge Share</h1>

</div>
</div>


</div>
</div>
<div class="container">
<div class="row">
<div style="margin-top:3%;" class="col-md-8 col-md-offset-2 text-center">
<!--h1 class="weight-800 uppercase">Welcome to our Blog</h1-->
<ol class="breadcrumb">
  <li><a href="<?php echo bloginfo('url'); ?>">Home</a></li>
  <li class="active">Support</li>
</ol>
<hr/ style="margin:20px 0 0 0;">
</div>
</div>
<div class="row">
<!-- Blog Posts Section -->
<div class="col-md-9">
<!-- Blog Post -->

<?php foreach($getLoop as $getLoopDdata){
    $cate=get_the_category($getLoopDdata->ID);
  if($cate[0]->cat_ID==3){
 
    $image = wp_get_attachment_image_src( get_post_thumbnail_id( $getLoopDdata->ID ),  array(300,300) );
   $linkd=  get_post_permalink($getLoopDdata->ID) ;
  ?>
<div class="row blog_content">
<div class="col-md-6">
<div class="blog-img-wrapper">
<div class="blog-img-hover">
<div class="hover-left"><a href="<?php echo $linkd;?>"><!--i class="im-redo2"></i--></a></div>
<div class="hover-right"><a data-pp="prettyPhoto[blog-gallery]" href="<?php echo $image[0]; ?>" title="CoeqWEty"><!--i class="im-expand2"></i--></a></div>
</div>
<img alt="" src="<?php echo $image[0]; ?>" class="img-responsive">
</div>
</div>
<div class="col-md-6">
<h4><a href="<?php echo $linkd;?>"><?php echo $getLoopDdata->post_title;?></a></h4>
<h5>by CoeqWEty on <?php $d=  strtotime($getLoopDdata->post_date); echo date('F d, Y',$d); ?></h5>
<p>
<?php 
$string = strip_shortcodes($getLoopDdata->post_content);

if (strlen($string) > 500) {
	
	

    $stringCut = substr($string, 0, 500);

    

    $string = substr($stringCut, 0, strrpos($stringCut, ' ')).'...'; 

} echo $string; ?> </p>
<?php /* <h5>Posted in Blog | 23 Comments</h5> */ ?>
</div>
<div class="col-md-12">
<hr/>
</div>
</div>
<?php
}

 }
?>
 
 
 <?php /*
<div class="row">
<div class="col-md-12">
<ul class="pagination">
  <li><a href="#">Previous</a></li>
  <li><a href="#">1</a></li>
  <li class="active"><a href="#">2</a></li>
  <li><a href="#">3</a></li>
  <li><a href="#">4</a></li>
  <li><a href="#">5</a></li>
  <li><a href="#">Next</a></li>
</ul>
</div>
</div> */ ?>
</div>

<?php get_sidebar(); ?>


</div>
</div>
</div>


<?php get_footer(); ?>