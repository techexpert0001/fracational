<?php

/**
 * The template part for displaying content
 * Template Name: Video
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
get_header();
?>

<?php /* <h2>video</h2> 
  <div class="container">
  <div class="top_bar">
  <div class="row">
  <div class="col-sm-2 logo_l ae" data-animation="flipInY" data-offset="70%"> <img src="/Content/themeforest/img/logo_s.png" /></div>
  <div class="col-sm-6 pull-right">
  <div class="welcome_right_top text-right">
  <ul>
  <li class="color-dark"><a href="javascript:void(0)"> WELCOME </a></li>
  <li class="color-dark"><a href="javascript:void(0)"> JOIN US </a></li>
  </ul>
  </div>
  </div>
  </div>
  </div>
  </div> */ ?>

<section id="video">
    <div class="container">
        <div class="row m_t" style="margin-bottom: 40px;">
            <div class="col-md-7 col-md-offset-2 text-center">
                <h1 class="weight-800 uppercase ae" data-animation="fadeInUp" data-offset="70%">How to Tutorials</h1>
                <h4 class="weight-400 ae" data-animation="flipInY" data-offset="70%" data-delay="500">The video series describes how you can easily participate in asset co-ownership and reap the benefits of shared ownership. Additionally, the how-to series demonstrates using the CoeqWEty mobile application to jump start your dreams today.</h4>
            </div>
        </div>
    </div>
    <div class="container video_title m_b" >
        <div id="masonry-grid">    
            <h2></h2>          
			<div class="col-md-6 grid-item m_b">
                <h4>Goals</h4>
                <p>A goal is your dream. You may have many dreams. A goal is a  co-ownership asset objective you create that has characteristics and provisions. A goal can be real property such as a vacation home or personal property such as a ski boat. Once created, you can discover people that have similar goals and profiles and invite them to a form a group. A group is a collection of members working towards the same dream to realize it. This video shows you how to create a goal from the Goal toolbar or Dashboard.  You can also use Goal Express from the dashboard that quickly creates a corresponding goal, that defaults many parameters that you may change.</p>
                <div class="video">
                    <!--iframe class="embed-responsive-item" src="https://www.youtube.com/embed/aZpjqBNSUCc?rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe-->					
					<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/MpyS6QhI76k?rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>
					<!--iframe class="embed-responsive-item" src="https://www.youtube.com/embed/MpyS6QhI76k?rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe-->
                </div>
            </div>			
            <div class="col-md-6 grid-item m_b">
                <h4>Profile</h4>
                <p>Your profile is initially established from the information from the social provider you elected to authenticate with and share. From there, you can update information regarding you, your family, and your payment methods. The most important information in your profile is your primary home location and your legal name, signature, and payment method if you plan to join groups. You can set privacy preferences to only share publicly only those items you wish too. This video describes the important items in the profile and how to complete your profile form the toolbar or Dashboard.</p>
                <div class="video">
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/DjrMt9H3pqs?rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>
                    <!--iframe class="embed-responsive-item" src="https://www.youtube.com/embed/Pjl0iOdQC5I?rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe-->
                </div>				
            </div>			
            <div class="col-md-6 grid-item m_b">
                <h4>Scheduling a Vote</h4>
                <p>Groups vote on all important objectives, provisions, and restrictions in property acquisition and operation. Voting events may have multiple measures to cast votes on. Voting events are generated and collected usually before the preparation of a legal agreement and the next stage in the asset lifecycle. This video shows how an owner/admin initiates a vote, how members vote, and how they review voting results</p>
                <div class="video">
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/ZXoELJ-IjfA?rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>
					
					
                </div>
            </div>			
			<div class="col-md-6 grid-item m_b">
                <h4>Voting</h4>
                <p>Groups vote on all important objectives, provisions, and restrictions in property acquisition and operation. Voting events may have multiple measures to cast votes on. Voting events are generated and collected usually before the preparation of a legal agreement and the next stage in the asset lifecycle. This video shows how an owner/admin initiates a vote, how members vote, and how they review voting results</p>
                <div class="video">
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/JZ3AOKZakUk?rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>
                    <!--iframe class="embed-responsive-item" src="https://www.youtube.com/embed/CbS9u0XA3E8?rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe-->
                </div>
            </div>
            <div class="col-md-6 grid-item m_b">
                <h4> Dashboard </h4>
                <p>On logging in and authentication, your home page is the dashboard. It allows you to quickly assess some metrics and quickly perform some actions on goals and groups. The navigation toolbar as well the dashboard allows you to access your Profile, Goals, Groups, and Workflow. The side Menu allows you to invite friends, become a partner, view documents uploaded or generated, and send Feedback. This video shows an overview of the Dashboard and Home page.</p>
                <div class="video">						
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/VDMg6aNn_WQ?rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>
                    <!--iframe class="embed-responsive-item" src="https://www.youtube.com/embed/CbS9u0XA3E8?rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe-->
                </div>
            </div>
			<div class="col-md-6 grid-item m_b">
                <h4> Administer </h4>
                <p>A group is formed from a goal. The group is initialized by whomever first creates the group. As members join a group, voting events can change the characteristics, financial, and ownership provisions of the group. Additionally, acquisition and operating budgets are established for assets and then voted upon. There are many items that an owner/admin of a group are responsible for. They lead the progress to realizing the groups’ collective dreams. This video also shows group members how to view the results of such actions.</p>
                <div class="video">
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/NXXibfyaD4M?rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>
					
					<!--iframe class="embed-responsive-item" src="https://www.youtube.com/embed/9E_mCrQKq5A?rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe-->
                </div>
            </div>
			<div class="col-md-6 grid-item m_b">
                <h4>Discovery</h4>
                <p>Discovery allows you to find people with similar goals/dreams. Your goals are available in a drop-down selection to find members with same dream. You can view their profile, their goals, and proprietary fit score. You can invite or message members in Discovery. This video shows you how to use Discovery.</p>
                <div class="video">
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/Tk5ny2M1yDM?rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>
                    <!--iframe class="embed-responsive-item" src="https://www.youtube.com/embed/jAfDsjo9Vag?rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe-->
                </div>
            </div>			
			<?php /*
            <div class="col-md-6 grid-item m_b">
                <h4>Groups</h4>
                <p>A group is formed from a goal. The group is initialized by whomever first creates the group. As members join a group, voting events can change the characteristics, financial, and ownership provisions of the group. Additionally, acquisition and operating budgets are established for assets and then voted upon. This video shows you the important parameters of a group and how to maintain a group as well as view group information</p>
                <div class="video">
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/aZpjqBNSUCc?rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>
                </div>
            </div> */ ?>			
			<?php /*
            <div class="col-md-6 grid-item m_b">
                <h4>Group control panel</h4>
                <p>There are many items that an owner/admin of a group are responsible for. They lead the progress to realizing the groups’ collective dreams. This video shows how an owner admin creates voting events, generates legal agreements, publishes usage schedules, controls group membership, and publishes properties to rental sharing platforms. This video also shows group members how to view the results of such actions.</p>
                <div class="video">
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/aZpjqBNSUCc?rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
			*/ ?>		
            <?php /* <div class="col-md-6 grid-item m_b">
                <h4>Partner Membership</h4>
                <p>Members can become partners if they so desire. There are strategic partners and local partners that are of various types. You can request to become a strategic partner and we will follow up with you as to fit. A local partner generally serves a specific locale and can provide added value servicesto the community in their field of specialization in that locale. For instance, this might include a realtor or a legal advisor. Local partners agree to a reasonable monthly subscription fee to be listed. All active partners are returned in results of Discovery so members can obtain assistance in owning and operating their dream. This video demonstrates becoming a partner on our platform.</p>
                <div class="video">
                    <iframe class="embed-responsive-item" src="https://www.youtube.com/embed/aZpjqBNSUCc?rel=0&showinfo=0" frameborder="0" allowfullscreen></iframe>
                </div>
            </div>
			*/ ?>
		</div>
    </div>
</section>
<?php get_footer(); ?>