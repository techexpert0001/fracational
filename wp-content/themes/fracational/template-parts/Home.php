<?php
/**
 * The template part for displaying content
 * Template Name: Homepage
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
get_header();
$useragent=$_SERVER['HTTP_USER_AGENT'];
?>
<section id="home">
    <?php /*<!--div class="container">
    <div class="top_bar">
        <div class="row">
            <div class="col-sm-2 logo_l ae" data-animation="flipInY" data-offset="70%"> <img src="<?php echo bloginfo('template_url');?>/assets/img/logo_s.png" /></div>
            <div class="col-sm-6 pull-right">
                <div class="welcome_right_top text-right">
                    <ul>
                        <li><a href="javascript:void(0)"> WELCOME </a></li>
                        <li><a href="javascript:void(0)"> JOIN US </a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    </div--> */ ?>
    <div class="fullscreen-video height-100 img-overlay1">
        <video autoplay="autoplay" loop autobuffer="autobuffer" muted="muted" controls width="100%" height="100%" frameborder="0">
            <source src="<?php echo bloginfo('template_url');?>/assets/img/neuron.mp4" type="video/mp4" />
        </video>		
		<div class="mobile_view_img"></div>		
        <div class="container">
            <div class="video_content">
                <div class="row">
				<!--img alt="logo" style="z-index:1" class="img-responsive ae" data-animation="flip" data-offset="95%" data-speed="1000" id="" src="<?php echo bloginfo('template_url');?>/assets/img/logo_dark_nav.png"--> 
                    <div class="col-md-12 text-center m_t">                       
                        <h1 class="weight-800 uppercase color-white super-large-caption ae" data-animation="rotateIn" data-offset="70%">CoeqWEty </h1>						
                    </div>
                </div>
				<br> 
                <div class="row color-white">
                    <!-- class="col-md-8 col-md-offset-2 text-center"-->
					<div class="text-center">
                        <h1 class="color-white weight-300 ae italic_text" data-animation="slideInUp" data-offset="70%">Taking the friction out of fractional co-ownership &trade;</h1>
                    </div>
                </div>
            </div>
        </div>
         <!--*END CAPTION*--> 
    </div>
</section>
<section id="fracational">
    <div class="fullwidth-section">
        <div class="container">
            <div class="row" style="margin-bottom: 60px;">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <!--h3 class="weight-800 kill-top-margin uppercase ae t_b_20" data-animation="fadeInUp" data-offset="70%">	CoeqWEty IS A COMMUNITY PLATFORM TO SEAMLESSLY INVEST, SELL, USE, EXCHANGE, AND LEASE CO-OWNERSHIP ACTIVE LIFESTYLE ASSETS.</h3--> 
					
                    <h3 class="weight-800 kill-top-margin uppercase ae t_b_20" data-animation="fadeInUp" data-offset="70%">	CoeqWEty IS A COMMUNITY PLATFORM TO SEAMLESSLY INVEST, SELL, USE, EXCHANGE, OR LEASE VACATION HOMES, INVESTMENTS, AND ACTIVE LIFESTYLE ASSETS. </h3> 
					
                    <!--h4 class="weight-400 ae" data-animation="flipInY" data-offset="70%" data-delay="250">Our purpose is to massively enable co-ownership of vacation homes and active lifestyle assets to fulfill your dreams and rejuvenate your soul. Members can seamlessly discover other parties with common dreams, collaborate on target properties with common characteristics, identify properties for acquisition, agree on operating ownership usage, restrictions, and conditions, and close on identified properties. The ventures can choose to automatically impound servicing costs, establish and secure rotation rights allocation, establish and enforce conditions and restrictions of co-ownership, and sell or exchange the investment interests.</h4-->

                    
                    <h4 class="weight-400 ae" data-animation="flipInY" data-offset="70%" data-delay="250"> CoeqWEty's purpose is to massively asset co-ownership ventures to realize your dreams and rejuvenate your soul. Members can seamlessly discover other parties with common dreams, collaborate on target properties with common characteristics, identify properties for acquisition, agree on operating ownership usage, restrictions, and conditions, and close on identified properties. </h4>                    
                </div>
            </div>
        </div>
    </div>
     <!--*END fracational*-->
</section>
<section id="today_problem">
    <div class="fullwidth-section">		
	
      <?php /*  <!--div class="parallax img-overlay3" style="background-image: url('<?php echo bloginfo('template_url');?>/assets/img/pictures/o-VACATION-HOME-facebook.gif');" data-stellar-background-ratio="0.2"></div--> */ ?>
        <div class="parallax img-overlay3" <?php 

if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)))
 
{
 ?>
  style="background: url('<?php echo bloginfo('template_url');?>/assets/img/pictures/o-VACATION-HOME-facebook.gif');"
<?php }else{
   ?>
             style="background-image: url('<?php echo bloginfo('template_url');?>/assets/img/pictures/o-VACATION-HOME-facebook.gif');"
   <?php  
}
        ?> data-stellar-background-ratio="0.5"></div>
		
        <div class="img-overlay-solid" style="background-color:rgba(60,62,71,0.5);"></div>
        <div class="container">
            <div class="row" style="margin-bottom: 60px;">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <!--h1 class="weight-800 kill-top-margin uppercase color-white ae" data-animation="fadeInUp" data-offset="70%">Today's Problem</h1-->
                    <h1 class="weight-800 kill-top-margin uppercase color-white ae t_b_20" data-animation="fadeInUp" data-offset="70%">TODAY’S PROBLEM</h1>
                    <h4 class="weight-400 color-white ae" data-animation="flipInY" data-offset="70%" data-delay="250">People need to rejuvenate their souls in this high-pressure world. Vacation home and active lifestyle assets typically suffer from poor utilization with high recurring and opportunity costs. These issues imped your ability to continually refresh. Alternative models of ownership present their own challenges.  We provide the perfect economic model to allow you to re-energize.</h4>
                </div>
            </div>
        </div>
    </div>
</section>
<section id="graph">
     <!--*SINGLE IMAGE TEAM DISPLAY*-->
    <div class="fullwidth-section">
        <div class="container">
            <div class="row" style="margin-bottom: 20px;">
                <div class="col-md-12">
                    <!--h1 class="weight-800 kill-top-margin uppercase">Our Team</h1-->
                    <h4 class="weight-400">The cost of vacation ownership can be high. There are several ways to have a second or third vacation home; however they have limited investment potential, have high recurring costs, are location limited, and/or unobtainable for the average family.</h4><br>
                    <h4 class="weight-400">There is strong demand for vacation home ownership. Vacation Homes are 12-15% of the total residential market with an aggregate market value $150 billion+ annually in the US alone. Personal ownership of vacation homes are typically within 200 miles of an owner’s primary residence. Currently, these are predominantly owned by a single owner. The cost and management of a vacation home ownership can be a hurdle and a burden. </h4>
                    <!--h4 class="weight-400">Active lifestyle assets such as boats, RV’s, motorcycles, small craft vehicles, and others many times suffer even poorer economics with a depreciating asset. The alternative here is primarily to rent/lease over buy. </h4-->
                    <h4 class="weight-400">Active lifestyle assets such as boats, RV’s, motorcycles, small craft vehicles, and others many times suffer even poorer economics with a depreciating asset. </h4>
					<br>
					
					<div class="col-md-12 text-center">
                    <img src="<?php echo bloginfo('template_url');?>/assets/img/graph.png" />
                </div>
					
                    <h4 class="weight-400">For vacation home ownership, there are other options to relieve this burden and provide value to others through utilizing home sharing rental services. It is estimated that the average current vacation home is made available for lease most the year. Owners can reduce costs, increase asset utilization and potentially gain investment returns through these services as well.  For personal property, the alternative is usually rent/lease over buy. </h4>
                    <!--h4 class="weight-400">For vacation home ownership, there are other options to relieve this burden and provide value to others through utilizing home sharing rental services. Home sharing lacks pride of ownership and is also subject to increasing local legal regulation with substantial fines. For personal property, the alternative is usually rent/lease over buy. </h4-->
                </div>
            </div>
            <div class="row ae" data-animation="fadeInUp" data-offset="70%" style="margin-bottom: 20px;">
                <div class="col-md-12">
                    <!--h4 class="weight-400"> It is estimated that the average current vacation home is made available for lease most the year. Owners can reduce costs, increase asset utilization and potentially gain investment returns through these services as well. </h4-->
                </div>
                <!--div class="col-md-5">
                    <img src="<?php //echo bloginfo('template_url');?>/assets/img/graph.png" />
                </div-->
            </div>
            <div class="row" style="margin-bottom: 20px;">
                <div class="col-md-12">
                   <?php /* <!--h1 class="weight-800 kill-top-margin uppercase">Our Team</h1--> */ ?>
					<h4 class="weight-400">What if there was a better way to increase asset utilization and cost efficiency while having vested ownership interest with all its benefits? <b>Thats where we come in.</b> </h4>
                </div>
            </div>
        </div>
    </div>    
</section>
<section id="where_we">
    <div class="fullwidth-section">
       <?php /*  <!--div class="parallax img-overlay3" style="background-image: url('<?php echo bloginfo('template_url');?>/assets/img/pexels-photo.gif');" data-stellar-background-ratio="0.2"></div--> */ ?>
        <div class="parallax img-overlay3" <?php 

if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)))
 
{
 ?>
  style="background: url('<?php echo bloginfo('template_url');?>/assets/img/pexels-photo.gif');"
<?php }else{
   ?>
            style="background-image: url('<?php echo bloginfo('template_url');?>/assets/img/pexels-photo.gif');"
   <?php  
   }
        ?> data-stellar-background-ratio="0.5"></div>		 
        <div class="img-overlay-solid"  style="background-color:rgba(60,62,71,0.5);"></div>
        <div class="container">
            <div class="row" style="margin-bottom: 60px;">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <h1 class="weight-800 kill-top-margin uppercase color-white ae t_b_20" data-animation="fadeInUp" data-offset="70%"> Where we come in </h1>
                    <h4 class="weight-400 color-white ae" data-animation="flipInY" data-offset="70%" data-delay="250">     Our passion is to enable you to relax, rejuvenate, and enjoy property today, while enjoying investment returns for your tomorrows. The better way is to enable mass adoption of co-ownership of vacation property and active lifestyle assets. Co-ownership with platform enabled cornerstones of built-in trust, a vested interest in asset protection, shared returns, and limited risk.</h4>
                    <!--h4 class="weight-400 color-white ae" data-animation="flipInY" data-offset="70%" data-delay="250">     However, home sharing lacks pride of ownership and is also subject to increasing local legal regulation with substantial fines.</h4-->                    
                </div>
            </div>
        </div>
    </div>
</section>

<section id="dream">
    <div class="fullwidth-section">
        <div class="container">
            <div class="row" style="margin-bottom: 40px;">
                <div class="col-md-8 col-md-offset-2 text-center">
                </div>
            </div>
            <div class="row">
                <div class="col-md-4 col-sm-12 col-xs-12 ae" data-animation="flipInY" data-offset="70%">
                    <h3 class="weight-800 kill-top-margin uppercase" data-animation="rotateIn" data-offset="70%">dream</h3>
					<br>
                    <h4 class="weight-400">You establish a dream by creating a goal. A goal is a multivalued value set of attributes related to its goal type.</h4>
					<br>
				<h4 class="weight-400">	It can be a real property goal like a vacation home or an active lifestyle goal like owning A ski boat A group is formed through discovery and  is a collection of members actively participating a specific target goal to realize that dream.</h4>
					<br>
					<div class="ae" data-animation="flipInY" data-offset="70%">				
						<img src="<?php echo bloginfo('template_url');?>/assets/img/dream_b.png" />
					</div>					
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12 ae" data-animation="flipInY" data-offset="70%" data-delay="250">
                    <h3 class="weight-800 kill-top-margin uppercase">discover</h3>
					<br>
                    <h4 class="weight-400">Discover families and potential partners who are compatible based upon several factors that are important to you.</h4>
					<br>
                    <h4 class="weight-400">Factors include: type of asset, target goal location, family profile, property characteristics, shared ownership provisions such as usage rights, method of title, level of impound accounts, restrictions on sales, exchanges and leasing. </h4><br>
                    <h4 class="weight-400">CoeqWEty enables trusted communications, workflow, agreement preparations and fund transfers for co-owners to realize their dreams. </h4>
					<br>
					<div class="ae" data-animation="flipInY" data-offset="70%" data-delay="250">					
                       <img src="<?php echo bloginfo('template_url');?>/assets/img/discover_b.png" />
					</div>
                </div>
                <div class="col-md-4 col-sm-12 col-xs-12 ae" data-animation="flipInY" data-offset="70%" data-delay="500">
                    <h3 class="weight-800 kill-top-margin uppercase">deal</h3>
					<br>
                    <h4 class="weight-400">Members of groups vote to align on various factors such as the property profile, ownership terms, and specific properties. Memorandum of Understanding and Co-ownership agreements are drawn up and digitally signed to form a basis of co-ownership that can be revised as necessary.</h4><br>
                    <h4 class="weight-400">Members can use our partner network to close and complete their investments or invite their own trusted partners(s) to the network. On- going servicing of the property enables usage scheduling, asset protection, and cost sharing funding enforcement and payment.</h4>
					<br>
					<div class="ae" data-animation="flipInY" data-offset="70%" data-delay="250">
                     <img src="<?php echo bloginfo('template_url');?>/assets/img/discover_b.png" />
                </div>
                </div>
            </div>
			<br>
         <?php /*    <!--div class="row">
                <div class="col-md-4 ae" data-animation="flipInY" data-offset="70%">
                   <img src="<?php echo bloginfo('template_url');?>/assets/img/dream_b.png" />
                </div>
                <div class="col-md-4 ae" data-animation="flipInY" data-offset="70%" data-delay="250">
                     <img src="<?php echo bloginfo('template_url');?>/assets/img/discover_b.png" />
                </div>
                <div class="col-md-4 ae" data-animation="flipInY" data-offset="70%" data-delay="500">
                    <img src="<?php echo bloginfo('template_url');?>/assets/img/deal_b.png" />
                </div>
            </div--> */ ?>
        </div>
    </div>
</section>
<?php /*
<!---section id="blog">
    <div class="fullwidth-section">
        <div class="container">
            <div class="row" style="margin-bottom:20px;">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <h1 class="weight-800 kill-top-margin uppercase ae" data-animation="fadeInUp" data-offset="70%">Blog</h1>
                    <h4 class="weight-400 ae" data-animation="flipInY" data-offset="70%" data-delay="500">Have a look and see what's new in our blog</h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 ae" data-animation="fadeInLeftBig" data-offset="70%">
                    <div class="blog-img-wrapper">
                        <div class="blog-img-hover">
                            <div class="hover-left"><a href="post-sidebar.html"><i class="im-redo2"></i></a></div>
                            <div class="hover-right"><a data-pp="prettyPhoto[blog-gallery]" href="<?php echo bloginfo('template_url');?>/assets/img/pictures/mountain.jpg" title="View the incredible mountains of Switzerland"><i class="im-expand2"></i></a></div>
                        </div>
                        <img alt="" src="<?php echo bloginfo('template_url');?>/assets/img/pictures/mountain.jpg" class="img-responsive">
                    </div>
                </div>
                <div class="col-md-3 ae" data-animation="fadeInLeftBig" data-offset="70%">
                    <h4 class="weight-700"><a href="post-sidebar.html">View the incredible mountains of Switzerland</a></h4>
                    <p>
                        Praesent augue arcu, ornare ut tincidunt eu, mattis a libero. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas...
                    </p>
                    <h6>by CreativelyCoded | 23 May 2014</h6>
                </div>
                <div class="col-md-3 ae" data-animation="fadeInRightBig" data-offset="70%">
                    <div class="blog-img-wrapper">
                        <div class="blog-img-hover">
                            <div class="hover-left"><a href="post-sidebar.html"><i class="im-redo2"></i></a></div>
                            <div class="hover-right"><a data-pp="prettyPhoto[blog-gallery]" href="<?php echo bloginfo('template_url');?>/assets/img/pictures/frog.jpg" title="The vibrantly colorful frogs of Costa Rica"><i class="im-expand2"></i></a></div>
                        </div>
                        <img alt="" src="<?php echo bloginfo('template_url');?>/assets/img/pictures/frog.jpg" class="img-responsive">
                    </div>
                </div>
                <div class="col-md-3 ae" data-animation="fadeInRightBig" data-offset="70%">
                    <h4 class="weight-700"><a href="post-sidebar.html">The vibrantly colorful frogs of Costa Rica</a></h4>
                    <p>
                        Praesent augue arcu, ornare ut tincidunt eu, mattis a libero. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas...
                    </p>
                    <h6>by CreativelyCoded | 23 May 2014</h6>
                </div>
            </div>
            <hr class="blank" />
            <div class="row">
                <div class="col-md-3 ae" data-animation="fadeInLeftBig" data-offset="70%">
                    <div class="blog-img-wrapper">
                        <div class="blog-img-hover">
                            <div class="hover-left"><a href="post-sidebar.html"><i class="im-redo2"></i></a></div>
                            <div class="hover-right"><a data-pp="prettyPhoto[blog-gallery]" href="<?php echo bloginfo('template_url');?>/assets/img/pictures/china.jpg" title="Travel on an exciting adventure to China"><i class="im-expand2"></i></a></div>
                        </div>
                        <img alt="" src="<?php echo bloginfo('template_url');?>/assets/img/pictures/china.jpg" class="img-responsive">
                    </div>
                </div>
                <div class="col-md-3 ae" data-animation="fadeInLeftBig" data-offset="70%">
                    <h4 class="weight-700"><a href="post-sidebar.html">Travel on an exciting adventure to China</a></h4>
                    <p>
                        Praesent augue arcu, ornare ut tincidunt eu, mattis a libero. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas...
                    </p>
                    <h6>by CreativelyCoded | 23 May 2014</h6>
                </div>
                <div class="col-md-3 ae" data-animation="fadeInRightBig" data-offset="70%">
                    <div class="blog-img-wrapper">
                        <div class="blog-img-hover">
                            <div class="hover-left"><a href="post-sidebar.html"><i class="im-redo2"></i></a></div>
                            <div class="hover-right"><a data-pp="prettyPhoto[blog-gallery]" href="<?php echo bloginfo('template_url');?>/assets/img/pictures/red-car.jpg" title="If you have to drive, cruise in style"><i class="im-expand2"></i></a></div>
                        </div>
                        <img alt="" src="<?php echo bloginfo('template_url');?>/assets/img/pictures/red-car.jpg" class="img-responsive">
                    </div>
                </div>
                <div class="col-md-3 ae" data-animation="fadeInRightBig" data-offset="70%">
                    <h4 class="weight-700"><a href="post-sidebar.html">If you have to drive, cruise in style</a></h4>
                    <p>
                        Praesent augue arcu, ornare ut tincidunt eu, mattis a libero. Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas...
                    </p>
                    <h6>by CreativelyCoded | 23 May 2014</h6>
                </div>
            </div>
        </div>
    </div>
</section--->
*/ ?>

<section id="features">
    <div class="fullwidth-section" style="background-color: #F5F5F5">
        <div class="container">
            <div class="row" style="margin-bottom: 40px;">
                <div class="col-md-7 col-md-offset-2 text-center">
                    <h1 class="weight-800 kill-top-margin uppercase ae t_b_20" data-animation="fadeInUp" data-offset="70%" data-delay="250">Our Features</h1>
                    <h4 class="weight-400 ae" data-animation="flipInY" data-offset="70%" data-delay="500"> Our  platform and community fulfills your dreams via end-to-end asset lifecycle support enabling all the benefits of the sharing economy</h4>
                    <!--h4 class="weight-400 ae" data-animation="flipInY" data-offset="70%" data-delay="500"> Actions required for co-ownership and include voting , responding to invites, and completing capital calls.</h4-->
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="icon-feature-horizontal ae" data-animation="bounceInLeft" data-offset="70%">
                        <div class="icon">
                            <i class="im-mobile color-primary"></i>
                        </div>
                        <div class="content">
                            <h4 class="uppercase weight-700">100% Built for Mobile</h4>
                            <p>
                                You are not tethered to your chair in todays world. You shop, bank, and finance all from your smartphone.
                            </p>
                        </div>
                    </div>
                    <div class="icon-feature-horizontal ae" data-animation="bounceInLeft" data-offset="70%">
                        <div class="icon">
                            <i class="im-copy color-primary"></i>
                        </div>
                        <div class="content">
                            <h4 class="uppercase weight-700">Multi-asset Goals</h4>
                            <p>
                                Our platform supports co-ownership acquisition and servicing of real and personal property  around active lifestyles.
                            </p>
                        </div>
                    </div>
                    <div class="icon-feature-horizontal ae" data-animation="bounceInLeft" data-offset="70%">
                        <div class="icon">
                            <i class="im-search color-primary"></i>
                        </div>
                        <div class="content">
                            <h4 class="uppercase weight-700">Co-owner Discovery and Fit</h4>
                            <p>
                                CoeqWEty allows you to bring your own members or find potential co-owners. We present a fit score based upon member profiles, lifestyle, and desired property features and provisions.
                            </p>
                        </div>
                    </div>
                    <div class="icon-feature-horizontal ae" data-animation="bounceInLeft" data-offset="70%">
                        <div class="icon">
                            <i class="im-copy color-primary"></i>
                        </div>
                        <div class="content">
                            <h4 class="uppercase weight-700">Provide Core Ownership Provisions</h4>
                            <p>
                                Every co-ownership model requires options on key provisions. Our platform provides many options on how to share usage, how to hold title, annual impounds, sales/exchange/ rental restrictions.
                            </p>
                        </div>
                    </div>
                    <div class="icon-feature-horizontal ae" data-animation="bounceInLeft" data-offset="70%">
                        <div class="icon">
                            <i class="im-list color-primary"></i>
                        </div>
                        <div class="content">
                            <h4 class="uppercase weight-700">Event Workflow</h4>
                            <p>Actions required for co-ownership and include voting ,  responding to invites, and completing capital calls.
                            </p>
							
                        </div>
                    </div>
                </div>
                <div class="col-md-4 ae" style="margin-top: 40px" data-animation="bounceInDown">
                    <img alt="" class="img-responsive img-center" src="<?php echo bloginfo('template_url');?>/assets/img/mobile_feature.png">
                </div>
                <div class="col-md-4">
                    <div class="icon-feature-horizontal ae" data-animation="bounceInRight" data-offset="70%">
                        <div class="icon">
                            <i class="im-quill color-primary"></i>
                        </div>
                        <div class="content">
                            <h4 class="uppercase weight-700">E-Vote for Mutual Agreement</h4>
                            <p>
                                A co-ownership group can vote on items relevant to the stage of a property acquisition. Items include property characteristics, ownership provisions, partners, funding, acquisition and operating budgets.
                            </p>
                        </div>
                    </div>
                    <div class="icon-feature-horizontal ae" data-animation="bounceInRight" data-offset="70%">
                        <div class="icon">
                            <i class="im-copy color-primary"></i>
                        </div>
                        <div class="content">
                            <h4 class="uppercase weight-700">
                                Electronic Document Generation with Digital Signatures
                            </h4>
                            <p>
                                Various  Co-ownership Legal Agreements such as a Memorandum of Understanding and Joint Ownership Agreements are prepared automatically based upon the lifecycle.
                            </p>
                        </div>
                    </div>
                    <div class="icon-feature-horizontal ae" data-animation="bounceInRight" data-offset="70%">
                        <div class="icon">
                            <i class="im-users color-primary"></i>
                        </div>
                        <div class="content">
                            <h4 class="uppercase weight-700">Partners in Progress</h4>
                            <p>
                                CoeqWEty has  strategic partners that specialize in co-ownership models and local  partners which can facilitate realizing your dream. You can sign up to be a value add partner as well as  co-ownership member.
                            </p>
                        </div>
                    </div>
                    <div class="icon-feature-horizontal ae" data-animation="bounceInRight" data-offset="70%">
                        <div class="icon">
                            <i class="im-coin color-primary"></i>
                        </div>
                        <div class="content">
                            <h4 class="uppercase weight-700">Instant ACH Verification and  Automated  Deposits</h4>
                            <p>
                                The platform collects deposits in trust based upon property lifecycle stage. These deposits held in trust are important for everyone as it shows commitment to fulfill the joint goal along with the agreements. The platform can validate bank accounts instantly and make required deposits.
                            </p>
                        </div>
                    </div>
                </div>              
               </div>
        </div>
    </div>
     <?php /* <!--div class="fullwidth-section bg-dark half-padding">
        <div class="container">
            <div class="col-md-12 text-center">
                <h2 class="color-white weight-300 kill-top-margin">View all of our <strong>Astonishing</strong> features on the shortcodes page</h2>
                <a class="btn btn-primary" href="javascript:void(0)">View Shortcodes <i class="im-redo2"></i></a>
            </div>
        </div>
    </div--> */ ?>
</section>
<section id="about">
    <div class="fullwidth-section">
        <div class="container">
            <div class="row" style="margin-bottom: 20px;">

                <div class="col-md-8 col-md-offset-2 text-center">
                    <h1 class="weight-800 kill-top-margin uppercase ae" data-animation="fadeInUp" data-offset="70%"> Who are we and why do we care? </h1>
                    <h4 class="weight-400 ae" data-animation="flipInY" data-offset="70%" data-delay="250">We are passionate about win-win-win relationships. We believe and are on fire about:</h4>

                </div>
            </div>
            <!--=========*STRAT ICON FEATURE BOX*=========-->
            <div class="row">
                <div class="col-md-4">
                    <div class="icon-feature-horizontal">
                        <div class="icon">
                            <i class="im-users color-primary ae"></i>
                        </div>
                        <div class="content">
                            <h4 class="uppercase weight-700">Vast Asset Sharing</h4>
                            <p>Enabling the mass adoption of vacation homeownership and active lifestyle assets. Leverage  sharing economy  utilization and cost efficiencies along with reasonable fees. The average  vacation home is not that far from your primary home. It is more cost efficient than sole ownership or alternatives such as Destination or Private Residence  Clubs
							</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="icon-feature-horizontal">
                        <div class="icon">
                            <i class="im-coin color-primary ae" data-delay="250"></i>
                        </div>
                        <div class="content">
                            <h4 class="uppercase weight-700">Invest in Your Memories</h4>
                            <p>Buying real estate to enjoy  today for your financial future tomorrow. Enjoy a vacation home and invest for your child's college education while building fond memories. Real estate has proven to be one of the best investments over the long run.
							</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="icon-feature-horizontal">
                        <div class="icon">
                            <i class="im-wand color-primary ae" data-delay="500"></i>
                        </div>
                        <div class="content">
                            <h4 class="uppercase weight-700">Rejuevenate Your Soul</h4>
                            <p>Providing you the opportunity to continually rejuvenate and disconnect  in this high stress world. Go to that  lake house, ski condo, beach home you have always wanted while building equity. Waterski, snowmobile, sail, motocross…Enjoy your active lifestyle assets in a cost effective way that makes life a persistent vacation.
							</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="icon-feature-horizontal">
                        <div class="icon">
                            <i class="im-pie color-primary ae"></i>
                        </div>
                        <div class="content">
                            <h4 class="uppercase weight-700">Propel Market Growth</h4>
                            <p>Growing the of $150 Billion annually vacation home market by  having the right value added trusted platform with the perfect partners.
							</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="icon-feature-horizontal">
                        <div class="icon">
                            <i class="im-shield color-primary ae" data-delay="250"></i>
                        </div>
                        <div class="content">
                            <h4 class="uppercase weight-700">Minimize Your Risk</h4>
                            <p>Minimizing the risk to all co-owner parties  through mutual agreements and conservative financial principles.
							</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="icon-feature-horizontal">
                        <div class="icon">
                            <i class="im-leaf color-primary ae" data-delay="500"></i>
                        </div>
                        <div class="content">
                            <h4 class="uppercase weight-700">Empower Youth</h4>
                            <p>Targeting  opportunities for youth to apply what they know to what will grow. We  believe in inspiring and growing the youth of today to breed impactful socially minded leaders and entrepreneurs that value compassion over cash.
							</p>
                        </div>
                    </div>
                </div>
            </div>
            <!--*END ICON FEATURE BOX*-->
        </div>
    </div>
    <!--*FEATURES COUNTERS*-->

   <?php /* <!--div class="fullwidth-section">
        <div class="parallax img-overlay3" style="background-image: url('<?php echo bloginfo('template_url');?>/assets/img/pictures/skyscrapers.jpg')" data-stellar-background-ratio="0.2"></div>
        <div class="img-overlay-solid" style="background-color:rgba(60,62,71,0.5);"></div>
        <div class="container">

            <div class="row color-white counter-section">
                <div class="col-md-3 text-center">
                    <i class="im-users color-white"></i>
                    <div class="counter" data-end-count="9046" data-speed="2500">0</div>
                    <h4 class="color-white uppercase weight-600">Happy Customers</h4>
                </div>
                <div class="col-md-3 text-center">
                    <i class="im-facebook color-white"></i>
                    <div class="counter" data-end-count="723" data-speed="2500">0</div>
                    <h4 class="color-white uppercase weight-600">Facebook Likes</h4>
                </div>
                <div class="col-md-3 text-center">
                    <i class="im-twitter color-white"></i>
                    <div class="counter" data-end-count="2134" data-speed="2500">0</div>
                    <h4 class="color-white uppercase weight-600">Twitter Followers</h4>
                </div>
                <div class="col-md-3 text-center">
                    <i class="im-fire color-white"></i>
                    <div class="counter" data-end-count="819" data-speed="2500">0</div>
                    <h4 class="color-white uppercase weight-600">Font Icons</h4>
                </div>
            </div>
        </div>
    </div--> */ ?>
</section>

<section id="team">
    <!--SINGLE IMAGE TEAM DISPLAY*-->
    <div class="fullwidth-section">
        <div class="container">
            <div class="row" style="margin-bottom: 20px;">
                <div class="col-md-6 col-md-offset-3 text-center">
                    <h1 class="weight-800 kill-top-margin uppercase ae" data-animation="fadeInUp" data-offset="70%">Meet Our Team</h1>
                    <h4 class="weight-400 ae" data-animation="flipInY" data-offset="70%" data-delay="250">A company is only as strong as the team behind it. Meet our team and find out why we're passionate about what we do.</h4>
                </div>
            </div>
        </div>
        <?php /* <!--------div class="row text-center">
            <div class="team-wrapper">
                <!--*TEAM MEMBER 1*-->
                <!--------ul class="team-drop">
                    <li style="position: absolute; left: 16%; top: 55%;">
                        <i class="fa-plus"></i>
                        <ul>
                            <li>
                                <h4 class="weight-700">Todd Bryant</h4>
                                <h6 class="uppercase weight-700  color-primary">Graphic Designer</h6>
                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore. </p>
                                <div class="social-icons"><i class="im-twitter"></i><i class="im-pinterest"></i><i class="im-instagram"></i><i class="im-google-plus"></i></div>
                            </li>
                        </ul>
                    </li>
                </ul>
                <!--*TEAM MEMBER 2*-->
                <!--------ul class="team-drop">
                    <li style="position: absolute; left: 33%; top: 21%;">
                        <i class="fa-plus"></i>
                        <ul>
                            <li>
                                <h4 class="weight-700">Sarah Parker</h4>
                                <h6 class="uppercase weight-700  color-primary">Lead Designer</h6>
                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore. </p>
                                <div class="social-icons"><i class="im-twitter"></i><i class="im-pinterest"></i><i class="im-instagram"></i><i class="im-google-plus"></i></div>
                            </li>
                        </ul>
                    </li>
                </ul>
                <!--*TEAM MEMBER 3*-->
                <!--------ul class="team-drop">
                    <li style="position: absolute; left: 44%; top: 59%;">
                        <i class="fa-plus"></i>
                        <ul>
                            <li>
                                <h4 class="weight-700">Mike Anthony</h4>
                                <h6 class="uppercase weight-700  color-primary">Creative Director</h6>
                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore. </p>
                                <div class="social-icons"><i class="im-twitter"></i><i class="im-pinterest"></i><i class="im-instagram"></i><i class="im-google-plus"></i></div>
                            </li>
                        </ul>
                    </li>
                </ul><!--*ul*-->
                <!--*TEAM MEMBER 4*-->
                <!--------ul class="team-drop">
                    <li style="position: absolute; left: 65%; top: 52%;">
                        <i class="fa-plus"></i>
                        <ul>
                            <li>
                                <h4 class="weight-700">Katie Roberts</h4>
                                <h6 class="uppercase weight-700  color-primary">Customer Service</h6>
                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore. </p>
                                <div class="social-icons"><i class="im-twitter"></i><i class="im-pinterest"></i><i class="im-instagram"></i><i class="im-google-plus"></i></div>
                            </li>
                        </ul>
                    </li>
                </ul>
                <!--*TEAM MEMBER 5*-->
                <!--------ul class="team-drop">
                    <li style="position: absolute; left: 81%; top: 15%;">
                        <i class="fa-plus"></i>
                        <ul style="right:0">
                            <li>
                                <h4 class="weight-700">Mike Billson</h4>
                                <h6 class="uppercase weight-700  color-primary">Lead Developer</h6>
                                <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore. </p>
                                <div class="social-icons"><i class="im-twitter"></i><i class="im-pinterest"></i><i class="im-instagram"></i><i class="im-google-plus"></i></div>
                            </li>
                        </ul>
                    </li>
                </ul>
                <img src="<?php echo bloginfo('template_url');?>/assets/img/team/team.jpg" />
            </div>
        </div----------> */ ?>

    </div>
    <!-- End Single Image Team Display -->
        <!-- Start Slider Team Display -->
    <div class="">
        <div class="container">
            <div class="row">
                <div id="team-slider" class="owl-carousel">
                    <!--*start MEMBER *-->
                    <div class="team-wrapper ae" data-animation="flip" data-offset="70%">
                        <div class="team-img-wrapper">
                            <div class="team-img-wrapper-hover">
                                <div class="social-icons"><i class="im-twitter"></i><i class="im-pinterest"></i><i class="im-instagram"></i><i class="im-google-plus"></i></div>
                            </div>
                            <img alt="" src="<?php echo bloginfo('template_url');?>/assets/img/team/bob.jpg" class="img-responsive img-center">
                        </div>
                        <h4 class="weight-700">Bob</h4>
                        <h5 class="color-primary uppercase">Founder, Chief of Strategy and Product Development</h5>
                        <p>Bob leads the product strategy and roadmap and oversees all product development.</p>
                        <hr />
                    </div>

                    <!--*START MEMBER 4*-->
                    <div class="team-wrapper ae" data-animation="flip" data-offset="70%">
                        <div class="team-img-wrapper">
                            <div class="team-img-wrapper-hover">
                                <div class="social-icons"><i class="im-twitter"></i><i class="im-pinterest"></i><i class="im-instagram"></i><i class="im-google-plus"></i></div>
                            </div>
                            <img alt="" src="<?php echo bloginfo('template_url');?>/assets/img/team/Donsgu.jpg" class="img-responsive img-center">
                        </div>
                        <h4 class="weight-700">Dongsu</h4>
                        <h5 class="color-primary uppercase">Chief Architect and Partner Integration</h5>
                        <p>Dongsu leads the cloud, partner integration, and platform scalability.</p>
                        <hr />
                    </div>
                    <!--*START MEMBER 4*-->
                    <!--div class="team-wrapper ae" data-animation="flip" data-offset="70%">
                        <div class="team-img-wrapper">
                            <div class="team-img-wrapper-hover">
                                <div class="social-icons"><i class="im-twitter"></i><i class="im-pinterest"></i><i class="im-instagram"></i><i class="im-google-plus"></i></div>
                            </div>
                            <img alt="" src="<?php //echo bloginfo('template_url');?>/assets/img/team/Nevil.jpg" class="img-responsive img-center">
                        </div>
                        <h4 class="weight-700">Nevil</h4>
                        <h5 class="color-primary uppercase">Lead Cross Platform Product Developer</h5>
                        <p>Nevil leads the mobile cross development and user experience.</p>
                        <hr />
                    </div-->
						
						
						  <!--*START MEMBER 4*-->
                    <div class="team-wrapper ae" data-animation="flip" data-offset="70%">
                        <div class="team-img-wrapper">
                            <div class="team-img-wrapper-hover">
                                <div class="social-icons"><i class="im-twitter"></i><i class="im-pinterest"></i><i class="im-instagram"></i><i class="im-google-plus"></i></div>
                            </div>
                            <img alt="" src="<?php echo bloginfo('template_url');?>/assets/img/team/oscar.jpg" class="img-responsive img-center">
                        </div>
                        <h4 class="weight-700">Oscar</h4>
                        <h5 class="color-primary uppercase">Lead Cross Platform Product Developer</h5>
                        <p>Nevil leads the mobile cross development and user experience.</p>
                        <hr />
                    </div>

                    <!--*START MEMBER *-->
                    <!--div class="team-wrapper ae" data-animation="flip" data-offset="70%">
                        <div class="team-img-wrapper">
                            <div class="team-img-wrapper-hover">
                                <div class="social-icons"><i class="im-twitter"></i><i class="im-pinterest"></i><i class="im-instagram"></i><i class="im-google-plus"></i></div>
                            </div>
                            <img alt="" src="<?php echo bloginfo('template_url');?>/assets/img/team/Jeff.jpg" class="img-responsive img-center">
                        </div>
                        <h4 class="weight-700">Jeff</h4>
                        <h5 class="color-primary uppercase">Controller</h5>
                        <p>Jeff leads development of financial models,  capital investment, and authorizes and controls expenditures.</p>
                        <hr />
                    </div-->

                    <!--*START MEMBER *-->
                    <div class="team-wrapper ae" data-animation="flip" data-offset="70%">
                        <div class="team-img-wrapper">
                            <div class="team-img-wrapper-hover">
                                <div class="social-icons"><i class="im-twitter"></i><i class="im-pinterest"></i><i class="im-instagram"></i><i class="im-google-plus"></i></div>
                            </div>
                            <img alt="" src="<?php echo bloginfo('template_url');?>/assets/img/team/Milos.jpg" class="img-responsive img-center">
                        </div>
                        <h4 class="weight-700">Milos</h4>
                        <h5 class="color-primary uppercase">Lead of Creative Content</h5>
                        <p>Milos leads development of all graphic and video content.</p>
                        <hr />
                    </div>
					
					
					
					  <!--*START MEMBER 4*-->
                    <div class="team-wrapper ae" data-animation="flip" data-offset="70%">
                        <div class="team-img-wrapper">
                            <div class="team-img-wrapper-hover">
                                <div class="social-icons"><i class="im-twitter"></i><i class="im-pinterest"></i><i class="im-instagram"></i><i class="im-google-plus"></i></div>
                            </div>
                            <img alt="" src="<?php echo bloginfo('template_url');?>/assets/img/team/new_team.jpg" class="img-responsive img-center">
                        </div>
                        <h4 class="weight-700">Kyle Bub</h4>
                        <h5 class="color-primary uppercase">Lead Cross Platform Product Developer</h5>
                        <p>Nevil leads the mobile cross development and user experience.</p>
                        <hr />
                    </div>
					
					
                   <?php /* <!--*START MEMBER *-->                   
					
					
					
                    <!--*START MEMBER *-->
                    <!--div class="team-wrapper ae" data-animation="flip" data-offset="70%">
                        <div class="team-img-wrapper">
                            <div class="team-img-wrapper-hover">
                                <div class="social-icons"><i class="im-twitter"></i><i class="im-pinterest"></i><i class="im-instagram"></i><i class="im-google-plus"></i></div>
                            </div>
                            <img alt="" src="<?php echo bloginfo('template_url');?>/assets/img/team/dave.jpg" class="img-responsive img-center">
                        </div>
                        <h4 class="weight-700">Dave</h4>
                        <h5 class="color-primary uppercase">Advisor/Board Member</h5>
                        <p>
                            Dave was a co-founder of Convertro. Convertro  was acquired by  AOL in 2014 followed by Verizon. He assists with funding, search optimization, and strategy.
                        </p>
                        <hr />
                    </div-->
                    <!--*START MEMBER *-->
                    <!--div class="team-wrapper ae" data-animation="flip" data-offset="70%">
                        <div class="team-img-wrapper">
                            <div class="team-img-wrapper-hover">
                                <div class="social-icons"><i class="im-twitter"></i><i class="im-pinterest"></i><i class="im-instagram"></i><i class="im-google-plus"></i></div>
                            </div>
                            <img alt="" src="<?php echo bloginfo('template_url');?>/assets/img/team/Tom.jpg" class="img-responsive img-center">
                        </div>
                        <h4 class="weight-700">Tom</h4>
                        <h5 class="color-primary uppercase">Advisor</h5>
                        <p>
                            Tom is President of  Salient CRGT. Tom drives a $ 1/2 Billion revenue company in the defense sector. He led the acquisition of companies and the ultimate sale of CRGT to a private equity firm and  Salient. He provides advice on acquisitions and  security vulnerability and mitigation approaches.
                        </p>
                        <hr />
                    </div--> */ ?>
                </div>

            </div>
        </div>
    </div>
    <!--*END SLIDER TEAM DISPLAY*-->
</section>

<section id="contact">
    <div class="fullwidth-section">
        <div class="parallax img-overlay4" <?php 

if(preg_match('/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i',$useragent)||preg_match('/1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i',substr($useragent,0,4)))
 
{
	?>
  style="background: url('<?php echo bloginfo('template_url');?>/assets/img/join_us_bg.jpg');"
<?php }else{
		?>
            style="background-image: url('<?php echo bloginfo('template_url');?>/assets/img/join_us_bg.jpg')"
   <?php  
}
        ?> data-stellar-background-ratio="0.5"></div>
        <div class="img-overlay-solid" style="background-color:rgba(60,62,71,0.7);"></div>
        <div class="container">

            <div class="row" style="margin-bottom: 150px; margin-top:150px;">
                <div class="col-md-6 col-md-offset-3 text-center">
                    <h1 class="weight-800 kill-top-margin uppercase color-white"> Join Us </h1>
                </div>
            </div>
           
        </div>
    </div>
   
    <div class="fullwidth-section" style="background-color: #F5F5F5">
        <div class="container">
            <div class="row" style="margin-bottom: 20px">
                <div class="col-md-8 col-md-offset-2 text-center">
                    <h1 class="weight-800 kill-top-margin uppercase" data-animation="fadeInUp" data-offset="70%" data-delay="250">JOIN US in THE SILICON SOUTHBAY  </h1>
                   <?php /* <!--h4 class="weight-400">Call us, email us or stop by the office, we're always here for you!</h4--> */ ?>
                    <a class="scrollto" href="#formPage">
                        <button class="btn btn-primary btn-round" id="requestForm" type="button">
                            JOIN OUR LAUNCH
                        </button></a>
</div>
            </div>
            <div class="row">
                <div class="col-md-8">
                    <div class="map-wrapper">
                        <div id="map-canvas">
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <h4 class="uppercase weight-700" style="margin-top: 20px">INSPIRATION AT</h4>
                    <p>
                        <strong class="color-primary">CoeqWEty</strong><br />
                        565 Pier Avenue, <br />
                        Hermosa Beach, CA,<br />
                        90254
                    </p><hr />
                 <?php /*    <!--h4 class="uppercase weight-700">Give us a shout</h4-->
                    <!--p>
                        <a href="mailto:support@creativelycoded.com">support@creativelycoded.com</a><br />
                        P: 555-555-5555<br />
                        M: 444-444-4444
                    </p--> */ ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12 col-sm-12 col-xs-12" id="formPage">

                    <div class="row" style="margin-bottom: 20px">
                        <div class="col-md-8">

                            <h4 class="weight-700">Contact Us</h4>
							
                            <h4 class="weight-400">Thank you for learning about us. We want to hear from you.</h4>
                        </div>
                    </div>
                    <form method="post" id="formverifyrequest" role="form">
                        <div class="row">
                            <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <label>First Name </label>
                                    <input class="form-control" name="fname" id="fname" placeholder="" type="text" />
                                </div>
                            </div>														
                        </div>
						<div class="row">
                            <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <label>Last Name </label>
                                    <input class="form-control" name="lname" id="lname" placeholder="" type="text" />
                                </div>
                            </div>
                            </div>
                        <div class="row">
                            <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <label>What&#39;s your email address? </label>
                                    <input class="form-control" name="email" id="email" placeholder="" type="email" />
                                </div>
                            </div>
                        </div>						
						<div class="row">
                            <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <label> AppledID <i class="fa fa-info-circle fa-0x text-default apple_msg_alert_m" aria-hidden="true"></i> </label>
								<p style="display:none;" id="apple_msg_alert" class="alert alert-success fade in">To evaluate you for our Beta products, we need your AppleID. We need your AppleID  to provision our application via Apple Products (sorry IOS only betas). You accept and always maintain control of provisioning.</p>									
                                <input class="form-control" name="appleemail" id="apple_email" placeholder="" type="email" />
                                </div>
                            </div>
                        </div>
						
                        <div class="row">
                            <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <label>Subject </label>
                                    <input class="form-control" name="subject" id="subject" placeholder="" type="text" />
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12 col-xs-12">
                                <div class="form-group">
                                    <label>Message </label>
                                    <textarea class="form-control" id="message" name="message" placeholder="" rows="8"></textarea>
                                </div>
                                <div class="text-center">
                                    <br />
                                    <button class="btn btn-primary btn-round FormdataSend" id="requestForm" type="button">
                                        Submit
                                    </button><br>
                                    <div id="error">Please fill out all required fields</div>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="alert alert-success emailformresponse" style="display:none;">Successfully Send</div>
                </div>
            </div>
        </div>
    </div>
</section>
<?php get_footer(); ?>