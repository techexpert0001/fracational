<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>
<div class="fullwidth-section half-padding footer">
    <div class="container">
        <div class="row social-icons text-center">
            <div class="col-md-12">
                <a href="https://twitter.com/coeqwety"><i class="im-twitter"></i></a>

                <a href="https://www.facebook.com/Coeqwety/"><i class="im-facebook"></i></a>

                <img alt="" class="img-responsive footer-logo ae" data-animation="flip" data-offset="95%" data-speed="1000" src="<?php echo bloginfo('template_url'); ?>/assets/img/logo_footer.png">

                <a href="https://linkedin.com/company/coeqwety">
                    <i class="fa fa-linkedin" aria-hidden="true"></i>
                </a>
                <a href="https://www.instagram.com/coeqwety/"><i class="im-instagram"></i></a>
            </div>
        </div>
        <div class="row text-center" style="margin-top: 20px">
            <div class="col-md-12">
                &copy; <?php echo date('Y'); ?> All Rights Reserved by <a href="<?php echo bloginfo('url'); ?>"> <b style="color:white;"> coeqwety </b> </a>
            </div>
        </div>
    </div>
</div>

<div class="scrollup">
    <a class="scrolltotop" href="#"><i class="fa fa-angle-double-up"></i></a>
</div>

</body>
<?php 
if(!is_home() and !is_front_page()){
wp_footer();
}

?>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M3XD4M7"
                  height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
 
<script src="<?php echo bloginfo('template_url'); ?>/assets/js/jquery-1.11.0.min.js"></script>
<script src="<?php echo bloginfo('template_url'); ?>/assets/js/bootstrap.min.js"></script>
<script src="<?php echo bloginfo('template_url'); ?>/assets/js/jquery.easing.1.3.js"></script>
<script src="<?php echo bloginfo('template_url'); ?>/assets/js/stellar.js"></script>
<script src="<?php echo bloginfo('template_url'); ?>/assets/js/counter.js"></script>
<script src="<?php echo bloginfo('template_url'); ?>/assets/js/jquery.prettyPhoto.js"></script>
<script src="<?php echo bloginfo('template_url'); ?>/assets/js/jquery.nicescroll.min.js"></script>
<script src="<?php echo bloginfo('template_url'); ?>/assets/js/jquery.nicescroll.plus.js"></script>
<script src="<?php echo bloginfo('template_url'); ?>/assets/js/jquery.superslides.js"></script>
<script src="<?php echo bloginfo('template_url'); ?>/assets/js/owl.carousel.min.js"></script>
<script src="<?php echo bloginfo('template_url'); ?>/assets/js/jquery.isotope.min.js"></script>
<script src="<?php echo bloginfo('template_url'); ?>/assets/js/animation-engine.js"></script>
<script src="<?php echo bloginfo('template_url'); ?>/assets/js/validation.js"></script>
 
<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&key=AIzaSyCkBsDG_JwBpGNNMn1O78mJTbnsLHlOQdU"></script>

<script src="<?php echo bloginfo('template_url'); ?>/assets/js/google.map-settings.js"></script>
<script src="<?php echo bloginfo('template_url'); ?>/assets/js/style-switcher.js"></script>
<script src="<?php echo bloginfo('template_url'); ?>/assets/js/astonish.js"></script>
<script src="https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js"></script>


<script>
 <?php /*    (function (i, s, o, g, r, a, m) {
        i['GoogleAnalyticsObject'] = r;
        i[r] = i[r] || function () {
            (i[r].q = i[r].q || []).push(arguments)
        }, i[r].l = 1 * new Date();
        a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
        a.async = 1;
        a.src = g;
        m.parentNode.insertBefore(a, m)
    })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

    ga('create', 'UA-100886716-1', 'auto');
    ga('send', 'pageview'); */ ?>


    (function ($) {
        "use strict";
        $(window).load(function () {
            $('#slides').superslides({
                animation: 'fade',
                play: false,
                pagination: true,
            });
        });
    })(jQuery);

 
    var $container = jQuery('#masonry-grid');
 
    $container.masonry({
       
        itemSelector: '.grid-item'
    });

    $(document).ready(function () {


        $('#masonry-grid').imagesLoaded(function () {
            $('#masonry-grid').masonry({
                itemSelector: '.grid-item',
                isAnimated: true,
                // isFitWidth: true
            });
        });
		
	$('.apple_msg_alert_m').click(function () {
	$('#apple_msg_alert').toggle();
	
	});


	
    });
</script>		
</html>

