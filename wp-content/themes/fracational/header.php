<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
           
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
   <meta name="msvalidate.01" content="02E7E6688BF0AE984D063AC5331C0D33"/>
    <meta name="description" content="Fractional">
    <meta name="keywords" content="Co-ownership real property, vacation homes, real estate, fractional ownership, real estate investing, Fractionalagreement, Partners,  REIT, Pier-to Pier Funding">  
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="profile" href="http://gmpg.org/xfn/11">
         <link rel="shortcut icon" type="image/png" href="<?php echo bloginfo('template_url');?>/assets/img/fevicon.png">   
    
    <link href="<?php echo bloginfo('template_url');?>/assets/css/bootstrap.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo bloginfo('template_url');?>/assets/css/buttons.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo bloginfo('template_url');?>/assets/css/font-awesome.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo bloginfo('template_url');?>/assets/css/icomoon.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo bloginfo('template_url');?>/assets/css/parallax-slider.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo bloginfo('template_url');?>/assets/css/prettyPhoto.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo bloginfo('template_url');?>/assets/css/animate.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo bloginfo('template_url');?>/assets/css/astonish.css" rel="stylesheet" type="text/css" />
    <link href="<?php echo bloginfo('template_url');?>/assets/css/headers/light.css" rel="stylesheet" id="header-switch" type="text/css" />
    <link href="<?php echo bloginfo('template_url');?>/assets/css/color-schemes/purple.css" rel="stylesheet" id="style-switch" type="text/css" />
    <link href="<?php echo bloginfo('template_url');?>/assets/css/style.css" rel="stylesheet" type="text/css" />
  
      <?php
 // $basename="d";
  $basename=basename($_SERVER['REQUEST_URI'], '?' . $_SERVER['QUERY_STRING']);
  $siteurl="http://www.coeqwety.com/";
  ?>
       <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M3XD4M7');</script>
       <!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-100886716-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-100886716-1');
</script>
	<?php if ( is_singular() && pings_open( get_queried_object() ) ) : ?>
	<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">  
    
	<?php endif; ?>
	<?php  wp_head(); ?>
        
        
        <?php /*if($basename=='video.php'){?> 
 
<?php 
<style>
 
 
@-webkit-keyframes spin {
  0% { -webkit-transform: rotate(0deg); }
  100% { -webkit-transform: rotate(360deg); }
}

@keyframes spin {
  0% { transform: rotate(0deg); }
  100% { transform: rotate(360deg); }
}

#status {
	width:200px;
	height:200px;
	position:absolute;
	left:50%;  
	top:50%;  
	background-image:url(../img/status.gif); 
	background-repeat:no-repeat;
	background-position:center;
	margin:-100px 0 0 -100px;  
} 
</style>

 <div class="loader"><div id="status">&nbsp;</div></div> ?>
 <?php } */ ?>
 
<!-- ===== PAGE LOADER GRAPHIC start===== -->
 
<div id="pageloader">
	<div class="loader-img">
		<img alt="loader" src="<?php echo bloginfo('template_url');?>/assets/img/loader.gif" /> </div>
</div>
<!-- ===== PAGE LOADER GRAPHIC end ===== -->
</head>



<body data-offset="62" data-spy="scroll" data-target=".navbar" <?php body_class(); ?>> 

    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <!-- navbar-fixed-top nav-fadein -->
        <div class="container">
            <!--Brand and toggle get grouped for better mobile display-->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#main-menu">
                    <i class="fa fa-bars fa-fw"></i>
                </button>
                <a class="navbar-brand weight-900" href="<?php echo bloginfo('url');?>#home">
					<!--img alt="footer_logo" class="img-responsive" alt="" id="logo" src="<?php echo bloginfo('template_url');?>/assets/img/logo_dark_nav.png"-->
					<img alt="logo" class="img-responsive ae" data-animation="flip" data-offset="95%" data-speed="1000" id="logo" src="<?php echo bloginfo('template_url');?>/assets/img/logo_dark_nav.png">
				</a>
				
		    </div>
            <div class="collapse navbar-collapse" id="main-menu">
                <ul class="nav navbar-nav navbar-right">
                    <li><a class="scrollto" href="<?php if($basename=='video' or $basename=='blog'){echo $siteurl; } ?>#home">Home</a></li>
                    <li><a class="scrollto" href="<?php if($basename=='video' or $basename=='blog'){echo $siteurl; } ?>#about">About</a></li>
                    <li><a class="scrollto" href="<?php if($basename=='video' or $basename=='blog'){echo $siteurl; } ?>#team">Team</a></li>
                    <li><a class="scrollto" href="<?php if($basename=='video' or $basename=='blog'){echo $siteurl; } ?>#features">Features</a></li>
                    <!--li><a class="scrollto" href="<?php if($basename=='video' or $basename=='blog'){echo $siteurl; } ?>#blog">How Do I</a></li-->
                    <li><a class="scrollto" href="video">How to</a></li>
                    <li><a class="scrollto" href="blog">Support</a></li>
                    <li><a class="scrollto" href="<?php if($basename=='video' or $basename=='blog'){echo $siteurl; } ?>#contact">Join Us</a></li>
                    <li><div class="social-icons"><a target="_blank" href="https://twitter.com/coeqwety"><i class="im-twitter"></i></a><a target="_blank" href="https://www.facebook.com/Coeqwety/"><i class="im-facebook"></i></a><a target="_blank" href="https://www.instagram.com/coeqwety/"><i class="im-instagram"></i></a><a target="_blank" href=" https://plus.google.com/b/105794279179066570220/105794279179066570220"><i class="im-google-plus"></i></a><a href="https://linkedin.com/company/coeqwety">
                    <i class="fa fa-linkedin" aria-hidden="true"></i>
                </a></div></li>
                </ul>
            </div>
        </div>
    </nav>
