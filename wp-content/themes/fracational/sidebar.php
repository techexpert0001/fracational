<?php
/**
 * The template for the sidebar containing the main widget area
 *
 * @package WordPress
 * @subpackage Twenty_Sixteen
 * @since Twenty Sixteen 1.0
 */
?>


<!-- Sidebar Section -->
<div id="sidebar" class="col-md-3">
 
 <?php echo do_shortcode('[widgets_on_pages id="1"]'); ?>
    
 
 </div>


<?php /* if ( is_active_sidebar( 'sidebar-1' )  ) : ?>
	<aside id="secondary" class="sidebar widget-area" role="complementary">
		<?php dynamic_sidebar( 'sidebar-1' ); ?>
	</aside><!-- .sidebar .widget-area -->
<?php endif; */ ?>
